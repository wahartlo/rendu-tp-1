# IA04 - Rendu de TP
## Paul NIVOIX et Louis WAHART

Ce répertoire contient les fichiers constituant le premier rendu de **TP d'IA04**.
Le but de l'application est de créer un service de vote à distance en utilisant l'architecture **Rest** et le langage **Go**. 

## Installation du projet
Pour être lancé, le projet doit d'abord être cloné à partir du répertoire Git à l'aide des commandes :
``` 
git clone https://gitlab.utc.fr/wahartlo/rendu-tp-1
cd rendu-tp-1
```

## Lancement de l'application
L'application permet de créer un service de vote par serveur Rest. La commande ```go run``` permet de lancer l'application avec 3 paramètres requis :
- **nbrAgts** : permet de définir le nombre d'agents votants,
- **nbrAlts** : permet de définir le nombre d'alternatives total,
- **rule** : permet de définir la règle de vote. Les règles de vote prises en charge sont *borda*, *copeland*, *majority*, *stv*, *kemeny* et *approval*. Toute tentative d'utilisation d'une autre règle de vote renverrait une erreur au niveau du serveur.
**Exemple de commande de lancement de l'application :**<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;```go run .\cmd\launch-all-rest-agents\ 20 5 majority```
<br> Une fois l'application lancée, celle-ci va afficher les votes de chacun des agents votants (qui sont générés aléatoirement), puis le rangement final de toutes les alternatives (rangées dans l'ordre décroissant de "popularité") ainsi que le gagnant.
