package comsoc

import (
	"errors"
	"rendu-tp-1/vtypes"
)

func TieBreakFactory(altsOrder []vtypes.Alternative) func([]vtypes.Alternative) (vtypes.Alternative, error) {
	return func(alts []vtypes.Alternative) (bestAlt vtypes.Alternative, err error) {
		for _, pref := range altsOrder {
			for _, alt := range alts {
				if alt == pref {
					return alt, nil
				}
			}
		}
		err = errors.New("Did not find matching alternative")
		return
	}
}

func DefaultTieBreak(alts []vtypes.Alternative) (bestAlt vtypes.Alternative, err error) {
	if len(alts) == 0 {
		err = errors.New("List of best alternatives is empty")
		return
	}
	bestAlt = alts[0]
	for _, alt := range alts {
		if alt < bestAlt {
			bestAlt = alt
		}
	}
	return bestAlt, nil
}
